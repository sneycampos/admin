<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data list</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/008e2f78ff.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<? include 'includes/side-menu.php'; ?>

<div id="right-panel" class="right-panel">

    <? include 'includes/header.php'; ?>

    <div class="content container-fluid">
        <div class="card">
            <div class="card-header">
                Contas a Pagar
            </div>
            <div class="card-body">
                <table class="table table-hover table-striped table-sm">
                    <thead>
                        <tr>
                            <th width="5%" scope="col">ID</th>
                            <th width="15%" scope="col">C.Crédito</th>
                            <th scope="col" class="text-center">C.Custo</th>
                            <th width="25%" scope="col">Parceiro/Descrição</th>
                            <th scope="col" class="text-right">Valor</th>
                            <th scope="col" class="text-center">Competência</th>
                            <th scope="col" class="text-right">Valor Pago</th>
                            <th scope="col" class="text-center">Recibo/NF</th>
                            <th scope="col" class="text-center">Comp. Pgto.</th>
                        </tr>
                    </thead>
                    <tbody>
                    <? for($i=0;$i<5;$i++){ ?>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    <?=rand(1000,9999)?>
                                </a>
                            </th>
                            <td>01.01.01.01<br>Caixa Interno</td>
                            <td class="text-center">OB02</td>
                            <td>
                                <a href="#" target="_blank"><b>Silvio Ney Campos Godinho</b></a><br>
                                SALÁRIO 05/2018
                            </td>
                            <td class="text-right">R$ 22.900,00</td>
                            <td class="text-center">10/10/2010</td>
                            <td class="text-right">R$ 19.000,00<br>01/10/2010</td>
                            <td class="text-center">
                                <a href="#">
                                    <i class="fa fa-barcode" aria-hidden="true"></i> Recibo/NF
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="">
                                    <i class="fa fa-money" aria-hidden="true"></i> Pagamento
                                </a>
                            </td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script src="assets/js/main.js"></script>

</body>
</html>
