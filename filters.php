<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Dashboard - Home Page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/008e2f78ff.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<? include 'includes/side-menu.php'; ?>

<div id="right-panel" class="right-panel">

    <? include 'includes/header.php'; ?>

<!--    <div class="breadcrumbs">-->
<!--        <div class="col-sm-12">-->
<!--            <div class="page-header float-left">-->
<!--                <div class="page-title">-->
<!--                    <h1>Contas a Pagar</h1>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

    <div class="content container">
        <div class="card">
            <div class="card-header">
                Buscar Contas a Pagar
            </div>
            <div class="card-body">
                <form action="list.php" method="get">
                    <div class="form-row">
                        <div class="col-md-6">

                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label for="partner_id">Parceiro</label>
                                    <select name="partner_id" id="partner_id" class="form-control form-control-sm">
                                        <option value="">- escolha -</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label for="cost_center_id">Centro de Custo</label>
                                    <select name="cost_center_id" id="cost_center_id" class="form-control form-control-sm">
                                        <option value="">- escolha -</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label for="credit_chart_account_id">Conta de Crédito</label>
                                    <select name="credit_chart_account_id" id="credit_chart_account_id" class="form-control form-control-sm">
                                        <option value="">- escolha -</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label for="debit_chart_account_id">Conta de Débito</label>
                                    <select name="debit_chart_account_id" id="debit_chart_account_id" class="form-control form-control-sm">
                                        <option value="">- escolha -</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="invoice_date">Data de Competência</label>
                                    <input type="text" class="form-control form-control-sm" name="invoice_date" id="invoice_date">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="due_date">Data de Vencimento</label>
                                    <input type="text" class="form-control form-control-sm" name="due_date" id="due_date">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row mt-3">
                        <div class="form-group col-md-4">
                            <button class="btn btn-primary">Buscar</button>
                            <span class="btn btn-secondary" onclick="window.history.back();">Cancelar</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="content mt-3">

    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>

<script src="assets/js/main.js"></script>

</body>
</html>
