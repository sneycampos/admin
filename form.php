<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nova Conta a Pagar - Home Page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/008e2f78ff.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<? include 'includes/side-menu.php'; ?>

<div id="right-panel" class="right-panel">
    <? include 'includes/header.php'; ?>

<!--    <div class="breadcrumbs">-->
<!--        <div class="col-sm-12">-->
<!--            <div class="page-header">-->
<!--                <div class="page-title">-->
<!--                    <h1>Novo Lançamento</h1>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

    <div class="container content">
        <form action="" method="post">
            <div class="card">
                <div class="card-header">
                    Novo lançamento
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-8 ">
                            <label for="partner_id">Parceiro</label>
                            <select name="partner_id" id="partner_id" class="form-control form-control-sm" aria-required="true">
                                <option value="">- escolha -</option>
                                <option value="1">Silvio Ney Campos Godinho</option>
                                <option value="2">Neuton Cavalcante Godinho</option>
                                <option value="3">Francisco Ribeiro Campos Neto</option>
                                <option value="4">Edneython Campos Godinho</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4 ">
                            <label for="cost_center_id">Centro de Custo</label>
                            <select name="cost_center_id" id="cost_center_id" class="form-control form-control-sm" aria-required="true">
                                <option value="">- escolha -</option>
                                <option value="1">Centro de custo 1</option>
                                <option value="2">Centro de custo 2</option>
                                <option value="3">Centro de custo 3</option>
                                <option value="4">Centro de custo 4</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="credit_chart_account_id">Conta de Crédito</label>
                            <select name="credit_chart_account_id" id="credit_chart_account_id" class="form-control form-control-sm">
                                <option value="">- escolha -</option>
                                <option value="1">01.01.01.01 - Conta XYZ</option>
                                <option value="2">02.01.01.01 - Conta XYZ</option>
                                <option value="3">03.01.01.01 - Conta XYZ</option>
                                <option value="4">04.01.01.01 - Conta XYZ</option>
                                <option value="5">05.01.01.01 - Conta XYZ</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row mt-3">
                        <div class="form-group col-md-3 ">
                            <label for="payment_type_id">Tipo de Pagamento</label>
                            <select name="payment_type_id" id="payment_type_id" class="form-control form-control-sm" aria-required="true">
                                <option value="">- escolha -</option>
                                <option value="1">DOC</option>
                                <option value="2">TED</option>
                                <option value="3">Boleto</option>
                                <option value="4">Espécie</option>
                                <option value="5">Cheque</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group mr-2 col-md-3">
                            <label for="price">Valor devido</label>
                            <input type="text" name="price" id="price" class="form-control form-control-sm" aria-required="true">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group mr-2 col-md-3">
                            <label for="due_date">Data de Vencimento</label>
                            <input type="text" name="due_date" id="due_date" class="form-control form-control-sm" value=""/>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="invoice_date">Data de Competência</label>
                            <input type="text" name="invoice_date" id="invoice_date" class="form-control form-control-sm" value=""/>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-8 ">
                            <label for="description">Descrição</label>
                            <textarea name="description" id="description" rows="4" class="form-control form-control-sm"></textarea>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4 ">
                            <label for="invoice_doc">Documento Gerador <span class="small text-muted">(.pdf)</span>
                            </label>
                            <input type="file" class="form-control-file form-control-sm" name="invoice_doc" id="invoice_doc"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Dados de Pagamento</div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group mr-2">
                            <label for="amount_paid">Valor Pago</label>
                            <input type="text" name="amount_paid" id="amount_paid" class="form-control form-control-sm" value=""/>
                        </div>

                        <div class="form-group">
                            <label for="payment_date">Data de Pagamento</label>
                            <input type="text" name="payment_date" id="payment_date" class="form-control form-control-sm" value=""/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="debit_chart_account_id">Conta de Débito</label>
                            <select name="debit_chart_account_id" id="debit_chart_account_id" class="form-control form-control-sm">
                                <option value="">- escolha -</option>
                                <option value="1">01.01.01.01 - Conta XYZ</option>
                                <option value="2">02.01.01.01 - Conta XYZ</option>
                                <option value="3">03.01.01.01 - Conta XYZ</option>
                                <option value="4">04.01.01.01 - Conta XYZ</option>
                                <option value="5">05.01.01.01 - Conta XYZ</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="payment_doc">Comprovante de Pagamento <span class="small text-muted">(.pdf)</span> </label>
                            <input type="file" class="form-control-file form-control-sm" name="payment_doc" id="payment_doc"/>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="form-group col-md-12">
                            <button class="btn btn-success">Salvar</button>
                            <span class="btn btn-secondary" onclick="window.history.back();">Cancelar</span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="assets/js/main.js"></script>

</body>
</html>
