<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./index.php"><img src="images/logo.png" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./index.php"><img src="images/logo2.png" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="./index.php"> <i class="menu-icon fa fa-lg fa-home"></i>Página Inicial</a>
                </li>
                <h3 class="menu-title"></h3>

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-table"></i>
                        Contas a Pagar
                    </a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-table"></i><a href="form.php">Nova Conta</a></li>
                        <li><i class="fa fa-table"></i><a href="filters.php">Listar todas</a></li>
                    </ul>
                </li>

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-th"></i>
                        Solicitação de Pagamento
                    </a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="#">Lançar nova</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="#">Listar todas</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</aside>